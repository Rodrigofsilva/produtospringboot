package service;



import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import entity.Produto;
import repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Produto save(@Validated Produto produto){
		return produtoRepository.save(produto);
	}
	
	 public Optional<Produto> findById(Integer id){
	        return produtoRepository.findById(id);
	    }
	
	public Iterable<Produto> findAll(){
		return produtoRepository.findAll();
	}
	
	public void delete (Integer id){
		produtoRepository.deleteById(id);;
	}
	
}
