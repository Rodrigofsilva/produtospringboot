package main;

import org.springframework.boot.SpringApplication;

public class ProdutoEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProdutoEurekaServerApplication.class, args);
	}
}
