package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import entity.Produto;
import service.ProdutoService;

@Controller
@RequestMapping("/produto")
public class ProdutoController {
	
	 @Autowired
	    private ProdutoService produtoService;

	    @RequestMapping(value="/save",method = RequestMethod.POST)
	    public ResponseEntity<Produto> save(@RequestBody  Produto p){
	        return ResponseEntity.ok(produtoService.save(p));
	    }

	    @RequestMapping(value = "/{idProduto}", method = RequestMethod.GET)
	    public ResponseEntity<Produto> findById(@PathVariable("id") Integer id){
	        return ResponseEntity.ok(produtoService.findById(id));
	    }

	    @RequestMapping(method = RequestMethod.GET)
	    public ResponseEntity<Iterable<Produto>> findAll(){
	        return ResponseEntity.ok().body(produtoService.findAll());
	    }

	    @RequestMapping(value = "/excluir/{idProduto}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> delete(@PathVariable("idProduto") Integer id){
	        produtoService.delete(id);
	        return ResponseEntity.ok().build();
	    }
	}
	

