package server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootConfiguration
@EnableConfigServer
public class ProdutoConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProdutoConfigServerApplication.class , args);
	}
}
