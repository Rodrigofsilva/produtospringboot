package repository;

import org.springframework.data.repository.CrudRepository;

import entity.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

}
